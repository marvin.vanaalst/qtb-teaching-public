{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from modelbase.ode import Model, Simulator, mca"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Simplified glycolysis model in erythrocytes\n",
    "\n",
    "<img src='img/erythrocyte-glycolysis.svg'>\n",
    "\n",
    "A simplified model of glycolysis in erythrocytes (see figure above) is built with the following differential equations:\n",
    "\n",
    "$$\\begin{eqnarray}\n",
    "\\frac{dP13G}{dt} &=& 2v_\\mathrm{HK-PFK} - v_\\mathrm{P2GM} - v_\\mathrm{PGK} \\\\\n",
    "\\frac{dP23G}{dt} &=& v_\\mathrm{P2GM} - v_\\mathrm{P2Gase} \\\\\n",
    "\\frac{dPEP}{dt}  &=& v_\\mathrm{P2Gase} + v_\\mathrm{PGK} - v_\\mathrm{PK} \\\\\n",
    "\\frac{dAMP}{dt}  &=& -v_\\mathrm{AK} \\\\\n",
    "\\frac{dADP}{dt}  &=& 2v_\\mathrm{HK-PFK} - v_\\mathrm{PGK} - v_\\mathrm{PK} + v_\\mathrm{ATPase} + 2v_\\mathrm{AK} \\\\\n",
    "\\frac{dATP}{dt}  &=& -2v_\\mathrm{HK-PFK} + v_\\mathrm{PGK} + v_\\mathrm{PK} - v_\\mathrm{ATPase} - v_\\mathrm{AK}\n",
    "\\end{eqnarray}$$\n",
    "\n",
    "All rate equations but HK-PFK are assumed to follow simple mass-action kinetics. For the HK-PFK system a substrate inhibition by ATP is assumed. The equations are given by\n",
    "\n",
    "$$\\begin{eqnarray}\n",
    "v_\\mathrm{HK-PFK} &=& \\frac{k_\\mathrm{HK-PFK} \\cdot ATP}{1+\\left(\\frac{ATP}{K_\\mathrm{I,ATP}}\\right)^{n_H}} \\\\\n",
    "v_\\mathrm{P2GM} &=& k_\\mathrm{P2GM} \\cdot P13G \\\\\n",
    "v_\\mathrm{P2Gase} &=& k_\\mathrm{P2Gase} \\cdot P23G \\\\\n",
    "v_\\mathrm{PGK} &=& k_\\mathrm{PGK} \\cdot P13G \\cdot ADP \\\\\n",
    "v_\\mathrm{PK} &=& k_\\mathrm{PK} \\cdot PEP \\cdot ADP \\\\\n",
    "v_\\mathrm{AK} &=& k^+_\\mathrm{AK}\\cdot AMP \\cdot ATP - k^-_\\mathrm{AK}\\cdot (ADP)^2 \\\\\n",
    "v_\\mathrm{ATPase} &=& k_\\mathrm{ATPase}\\cdot ATP\n",
    "\\end{eqnarray}$$\n",
    "\n",
    "We further assume, that the adenylate kinase (AK) reaction is fast and approximately at equilibrium\n",
    "\n",
    "$$\\begin{equation}\n",
    "\\frac{(ADP)^2}{AMP\\cdot ATP} = q_\\mathrm{AK}\n",
    "\\end{equation}$$\n",
    "\n",
    "The system contatins the following moiety:\n",
    "\n",
    "$$\\begin{equation}\n",
    "AMP+ADP+ATP = A.\n",
    "\\end{equation}$$\n",
    "\n",
    "The standard parameters of the model are given in the table below\n",
    "\n",
    " \n",
    "$$\\begin{array}{c c}\n",
    "  \\mathrm{Parameter} & \\mathrm{Value} \\\\\n",
    "    k_\\mathrm{HK-PFK} & 3.2 \\ \\mathrm{h}^{-1} \\\\\n",
    "    k_\\mathrm{P2GM} & 1500 \\ \\mathrm{h}^{-1} \\\\\n",
    "    k_\\mathrm{P2Gase} & 0.15 \\ \\mathrm{h}^{-1} \\\\\n",
    "    k_\\mathrm{PGK} & 1.57\\cdot 10^4\\ \\ (\\mathrm{mM}\\cdot\\mathrm{h})^{-1} \\\\\n",
    "    k_\\mathrm{PK} & 559 \\ (\\mathrm{mM}\\cdot\\mathrm{h})^{-1} \\\\\n",
    "    k_\\mathrm{ATPase} & 1.46 \\ \\mathrm{h}^{-1} \\\\\n",
    "    n_H & 4.0 \\\\\n",
    "    K_\\mathrm{I,ATP} & 1.0 \\ \\mathrm{mM} \\\\\n",
    "    q_\\mathrm{AK} & 2.0 \\\\\n",
    "    k_{AK^+} & 4 \\\\\n",
    "    k_{AK^-} & 2 \\\\\n",
    "    A & 1.5 \\ \\mathrm{mM} \\\\\n",
    "\\end{array}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use these starting conditions for the tasks below\n",
    "\n",
    "```\n",
    "P13G: 0.005\n",
    "P23G: 5.0\n",
    "PEP: 0.02\n",
    "AMP: 0.476\n",
    "ADP: 0.92\n",
    "ATP: 0.1\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tasks\n",
    "\n",
    "* Plot the rate equation of $v_\\mathrm{HK-PFK}$ as a function of ATP in the interval $0 <= ATP <= 2$ mM\n",
    "* Repeat the previous task with the changed parameter values $k_\\mathrm{HK-PFK}=2.29\\ \\mathrm{h}^{-1}$ and $n_H=1.0$. Plot both rate equations and interpret the solution\n",
    "* Calculate (both analytically and numerically) and plot the elasticities of $v_\\mathrm{HK-PFK}$ depending on the substrate concentration. \n",
    "* Implement the model using the `modelbase` package\n",
    "* Calculate the metabolite concentrations in steady state\n",
    "* To make the substrate inhibition of the HK-PFK less cooperative, set $n_H=1$. To which value do you have to change the parameter $k_\\mathrm{HK-PFK}$ in order to get the same steady state as with the standard parameters?\n",
    "* Vary the rate constants of the ATP consumption $k_\\textrm{ATPase}$ systematically and plot the stationary ATP concentrations once as a function of the parameters and once as  a function of the ATP consumption rate $v_\\mathrm{ATPase}$ for both model variants\n",
    "* Plot the stationary flux $J=v_\\mathrm{HK-PFK}$ as a function of the parameter $k_\\textrm{ATPase}$\n",
    "* Calculate the flux control coefficients for the glycolytic flux $J=v_\\mathrm{HK-PFK}$ and the concentration control coefficients for the metabolites $ATP$ and $P23G$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.10"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
